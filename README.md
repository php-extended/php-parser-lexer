# php-extended/php-parser-lexer

This library is an implementation of the php-extended/php-parser-interface based on a lexer for handling its data.

![coverage](https://gitlab.com/php-extended/php-parser-lexer/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-parser-lexer/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-parser-lexer ^8`


## Basic Usage

This library provide a single class, the `AbstractParserLexer` which is
made to be implemented. Its `parseLexer` method must return a single object
from the whole string that its given, and throw a `ParseException` if it is
impossible to do so.

It is advised that the `parseLexer` method use a combination of :

```php

public function parseLexer(LexerInterface $lexer)
{
	$lexer->rewind(); // do not forget !
	$token = $this->expectOneOf($lexer, [<tokenids>]);
	$this->expectEof($lexer, $token);
	
	return MyObject($token->getData());
}
```

As the `LexerInterface` is an `\Iterable`, you may as well use it
into a foreach loop.


## License

MIT (See [license file](LICENSE)).
