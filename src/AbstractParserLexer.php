<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-lexer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Parser;

use PhpExtended\Lexer\LexemeInterface;
use PhpExtended\Lexer\LexerConfiguration;
use PhpExtended\Lexer\LexerConfigurationInterface;
use PhpExtended\Lexer\LexerFactory;
use PhpExtended\Lexer\LexerFactoryInterface;
use PhpExtended\Lexer\LexerInterface;

/**
 * AbstractParserLexer class file.
 * 
 * This class is a parser that is based on a lexer to get its data stream.
 * 
 * @author Anastaszor
 * @template T of object
 * @extends AbstractParser<T>
 * @implements ParserInterface<T>
 */
abstract class AbstractParserLexer extends AbstractParser implements ParserInterface
{
	
	/**
	 * The class this parser expects parse objects to.
	 *
	 * @var class-string<T>
	 */
	protected string $_class;
	
	/**
	 * The lexer factory.
	 *
	 * @var LexerFactoryInterface
	 */
	protected LexerFactoryInterface $_factory;
	
	/**
	 * The lexer configuration.
	 *
	 * @var LexerConfigurationInterface
	 */
	protected LexerConfigurationInterface $_config;
	
	/**
	 * Builds a new AbstractParserLexer with the given lexer components.
	 * 
	 * @param class-string<T> $parsedClass
	 * @param LexerFactoryInterface $factory
	 * @param LexerConfigurationInterface $configuration
	 */
	public function __construct(string $parsedClass, ?LexerFactoryInterface $factory = null, ?LexerConfigurationInterface $configuration = null)
	{
		$this->_class = $parsedClass;
		
		if(null === $factory)
		{
			$factory = new LexerFactory();
		}
		$this->_factory = $factory;
		
		if(null === $configuration)
		{
			$configuration = new LexerConfiguration();
		}
		$this->_config = $configuration;
	}
	
	/**
	 * Parses effectively the data as ipv4 data.
	 *
	 * @param LexerInterface $lexer
	 * @return T
	 * @throws ParseThrowable
	 */
	abstract public function parseLexer(LexerInterface $lexer);
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Parser\ParserInterface::parse()
	 */
	public function parse(?string $data) : object
	{
		try
		{
			return $this->parseLexer($this->_factory->createFromString((string) $data, $this->_config));
		}
		catch(ParseThrowable $e)
		{
			throw new ParseException($this->_class, (string) $data, $e->getOffset(), null, (int) $e->getCode(), $e);
		}
	}
	
	/**
	 * Gets the next lexeme if it is one of the lexemes for the given lexeme
	 * codes, or returns null if the lexeme is not the right kind. This method
	 * does not move forward the lexer if the token is not the right kind.
	 * 
	 * @param LexerInterface $lexer
	 * @param array<integer, integer> $lexemeCodes
	 * @return ?LexemeInterface
	 */
	public function maybeOneOf(LexerInterface $lexer, array $lexemeCodes) : ?LexemeInterface
	{
		if(!$lexer->valid())
		{
			return null;
		}
		
		$token = $lexer->current();
		
		if(!\in_array($token->getCode(), $lexemeCodes, true))
		{
			return null;
		}
		
		$lexer->next();
		
		return $token;
	}
	
	/**
	 * Gets the next lexeme if it is one of the lexemes for the given lexeme
	 * codes, or throws a ParseException of the lexeme is not the right kind.
	 * This method does move forward the lexer if the token even if it is not
	 * the right kind.
	 * 
	 * @param LexerInterface $lexer
	 * @param array<integer, integer> $lexemeCodes
	 * @param LexemeInterface $previousToken
	 * @return LexemeInterface
	 * @throws ParseThrowable
	 */
	public function expectOneOf(LexerInterface $lexer, array $lexemeCodes, ?LexemeInterface $previousToken = null) : LexemeInterface
	{
		if(!$lexer->valid())
		{
			$message = 'Unexpected end of stream when parsing {class} : expected one of {list}';
			$context = [
				'{class}' => $this->_class,
				'{list}' => $this->getTokenNameList($lexemeCodes),
			];
			$offset = null === $previousToken
				? 0
				: 1 + $previousToken->getColumn() + (int) \mb_strlen($previousToken->getData());
			
			throw new ParseException($this->_class, '', $offset, \strtr($message, $context));
		}
		
		$token = $lexer->current();
		$lexer->next();
		
		return $this->isOneOf($token, $lexemeCodes, $previousToken);
	}
	
	/**
	 * Evaluates the given lexeme if it is one of the lexemes for the given
	 * codes, or throws a ParseException of the lexeme is not the right kind.
	 * 
	 * @param LexemeInterface $token
	 * @param array<integer, integer> $lexemeCodes
	 * @param LexemeInterface $previousToken
	 * @return LexemeInterface
	 * @throws ParseThrowable
	 */
	public function isOneOf(LexemeInterface $token, array $lexemeCodes, ?LexemeInterface $previousToken = null) : LexemeInterface
	{
		if(\in_array($token->getCode(), $lexemeCodes, true))
		{
			return $token;
		}
		
		$message = 'Unexpected token {name} when parsing {class} from previous token {token} : expected one of {list}';
		$context = [
			'{name}' => $this->getTokenName($token->getCode()),
			'{class}' => $this->_class,
			'{token}' => $this->getTokenName(null === $previousToken ? null : $previousToken->getCode()),
			'{list}' => $this->getTokenNameList($lexemeCodes),
		];
		
		throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
	}
	
	/**
	 * Evaluates whether the given lexer is at EOF, and throws an exception if
	 * it is not.
	 * 
	 * @param LexerInterface $lexer
	 * @param LexemeInterface $previousToken
	 * @throws ParseThrowable
	 */
	public function expectEof(LexerInterface $lexer, ?LexemeInterface $previousToken = null) : void
	{
		if($lexer->valid())
		{
			$token = $lexer->current();
			$message = 'Unexpected token {name} when parsing {class} from previous token {token} : expected end of stream';
			$context = [
				'{name}' => $this->getTokenName($token->getCode()),
				'{class}' => $this->_class,
				'{token}' => $this->getTokenName(null === $previousToken ? null : $previousToken->getCode()),
			];
			
			throw new ParseException($this->_class, $token->getData(), $token->getColumn(), \strtr($message, $context));
		}
	}
	
}
