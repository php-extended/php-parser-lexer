<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-parser-lexer library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Lexer\LexerInterface;
use PhpExtended\Parser\AbstractParserLexer;
use PhpExtended\Parser\ParseException;
use PHPUnit\Framework\TestCase;

/**
 * AbstractParserLexerTest class file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Parser\AbstractParserLexer
 *
 * @internal
 *
 * @small
 */
class AbstractParserLexerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var AbstractParserLexer
	 */
	protected AbstractParserLexer $_object;
	
	public function testParse() : void
	{
		$this->assertEquals(new stdClass(), $this->_object->parse('aa'));
	}
	
	public function testParseFailed() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('----');
	}
	
	public function testParseEmpty() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('');
	}
	
	public function testParseNotEnough() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('a');
	}
	
	public function testParseMaybe() : void
	{
		$this->assertEquals(new stdClass(), $this->_object->parse('aaa'));
	}
	
	public function testParseTooMuch() : void
	{
		$this->expectException(ParseException::class);
		
		$this->_object->parse('aaaa');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new class() extends AbstractParserLexer
		{
			
			public const T_ALPHA = 1;
			
			public function __construct()
			{
				parent::__construct(stdClass::class);
				$this->_config->addMappings(LexerInterface::CLASS_ALNUM, self::T_ALPHA);
			}
			
			public function parseLexer(LexerInterface $lexer)
			{
				$lexer->rewind();
				$token = $this->expectOneOf($lexer, [self::T_ALPHA]);
				$token = $this->expectOneOf($lexer, [self::T_ALPHA], $token);
				$this->maybeOneOf($lexer, [2]);
				$token = $this->maybeOneOf($lexer, [self::T_ALPHA], $token);
				$this->expectEof($lexer, $token);
				
				return new stdClass();
			}
			
		};
	}
	
}
